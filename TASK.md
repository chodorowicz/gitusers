# Task

## Objective

Single Page application (SPA) which displays a list of Github users by using Github public API with an option to display detailed user data on a separate page.

## Stack

Following stack should be used in order to achieve the task:
- TypeScript
- Redux ([react-redux](https://github.com/reactjs/react-redux) and [redux](https://github.com/reactjs/redux))
- [React router](https://github.com/ReactTraining/react-router)
- HTTP library of any kind ([fetch](https://github.com/github/fetch) - included with create-react-app, [superagent](https://github.com/visionmedia/superagent) etc…)
- Test runner such as [Jest](https://facebook.github.io/jest/) or [Mocha](https://mochajs.org/) (Jest comes packed with create-react-app). You can use [enzyme](https://github.com/airbnb/enzyme) or p[react-test-utils](https://reactjs.org/docs/test-utils.html) for traversing the virtual dom.

## Final objective

- Project starts up correctly and compiles without errors.
- A list of github users (avatar, login, details button) is displayed.
  - Bonus points if you can show the loading spinner before all users have been fetched.
  - Bonus points if you can make “Load more users” feature
- When details button is clicked a new page with information about the user is shown. Fields (id, avatar, login, html_url) with a back button to go back to the initial list of users.
- Single user page should display the user data after refreshing the page.
- Provide a test for at least 1 react component and 1 reducer.
