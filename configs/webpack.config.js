const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const history = require("connect-history-api-fallback");
const convert = require("koa-connect");
const webpackServeWaitpage = require("webpack-serve-waitpage");


const {
  NODE_ENV
} = process.env;
const isProduction = NODE_ENV === "production";
const mode = NODE_ENV ? NODE_ENV : "development";

module.exports = {
  context: path.resolve("..", __dirname),
  mode,
  entry: {
    app: "../src/index.tsx",
  },
  output: {
    filename: isProduction ? "[name].[chunkhash].js" : "[name].js",
    path: path.resolve(__dirname, "..", "dist"),
    publicPath: '/',
  },
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx", "json"],
  },
  devtool: "source-map",
  devServer: {
    hot: true,
  },
  module: {
    rules: [{
        test: /\.(j|t)s(x?)$/,
        exclude: /node_modules/,
        use: {
          loader: "awesome-typescript-loader",
          options: {
            transpileOnly: true,
            useTranspileModule: true,
            /* Babel is needed for React-Hot-Loader to work
               > When using TypeScript, Babel is not required, but React Hot Loader will not work (properly) without it.
               > https://github.com/gaearon/react-hot-loader#typescript
             **/
            useBabel: true,
          }
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
    ]
  },
  optimization: {
    runtimeChunk: "single", // extract runtime into one separate chunk
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all"
        }
      }
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: '../public/index.html'
    }),
  ]
};

module.exports.serve = {
  content: [__dirname],
  add: (app, _middleware, options) => {
    app.use(convert(history()));
    app.use(webpackServeWaitpage(options));
  },
};
