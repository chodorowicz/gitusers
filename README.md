# Readme / instructions

## Task descriptions

[task description](TASK.md)

## Requirements

- recent version of node (e.g 10)
- npm package manager

## Running app locally (dev environment)

```sh
npm install
npm run dev
```

## Running tests

```sh
npm test
```

## Running webtests

The app has a basic suite of integration tests implemented using Cypress.io. Test run in Chrome browser.

```sh
npm run cypress:open
```

## Deploy

```sh
npm run build
node start
```
