import { Selector } from 'testcafe';

fixture('Users List').page('localhost:3000');
test('App loads users and load more users works properly', async (test) => {
  const rows = Selector('table tbody tr');
  const button = Selector('.App-loadMoreRow button')

  await test
    .expect(rows.count).eql(30)
    .click(button)
    .expect(rows.count).eql(60)
});

fixture('User details').page('localhost:3000');
test('App loads user page and goes back', async (test) => {
  const firstUserDetails = Selector('table tbody tr:first-child a.btn');
  const detailsPage = Selector('.Details');
  const goBackButton = Selector('.btn');
  const rows = Selector('table tbody tr');

  await test
    .click(firstUserDetails)
    .expect(detailsPage.count).eql(1)
    .click(goBackButton)
    .expect(rows.count).eql(30);
});

fixture('User details page loads user properly').page('localhost:3000/details/defunkt');
test('App loads user page and goes back', async (test) => {
  const detailsPageH3 = Selector('.Details h3');

  await test
    .expect(detailsPageH3.count).eql(1)
});
