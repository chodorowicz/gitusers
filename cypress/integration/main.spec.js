describe("Main page", function() {
  it("App loads users and load more users works properly", function() {
    cy.visit("/");
    cy.get("tbody").find("tr").should("have.length", 30);
    cy.get(".App-loadMoreRow").find("button").click();
    cy.get("tbody").find("tr").should("have.length", 60);
  });
});

describe("Details page", function() {
  it("App loads user page and goes back", function() {
    cy.visit("/");
    cy.get("tbody").find("tr").contains("Details").click();
    cy.get(".Details").should("have.length", 1);
    cy.contains('Go Back').click();
    cy.get("tbody").find("tr").should("have.length", 30);
  });

  it("App loads user page and goes back", function() {
    cy.visit("/details/defunkt");
    cy.contains("User: defunkt").should("have.length", 1);
  });
});
