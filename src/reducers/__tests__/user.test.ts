import user, { initialState } from '../user';
import { loadUserRequest, loadUserSuccess, loadUserFailure } from '../../actions/actionsCreators';
import { user as mockUser } from "../../../tests/__mocks__/user";

describe('user reducer', () => {
  it('loads user', () => {
    const nextState = user(initialState, loadUserRequest(""))
    expect(nextState).toMatchSnapshot()
  });
  it('successfully loads user', () => {
    const nextState = user(initialState, loadUserSuccess(mockUser))
    expect(nextState).toMatchSnapshot()
  });
  it('handles failure properly', () => {
    const nextState = user(initialState, loadUserFailure())
    expect(nextState).toMatchSnapshot()
  });
})
