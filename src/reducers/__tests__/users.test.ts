import users, { initialState } from '../users';
import { getUsersRequest, getUsersSuccess, getUsersFailure } from '../../actions/actionsCreators';
import { IStore } from '..';
import { user } from "../../../tests/__mocks__/user";

describe('users reducer', () => {
  const payload = {
    data: [
      user,
      user,
    ],
    nextLink: "",
  };

  let stateAfterSuccessfullLoad: IStore["users"];
  it('sets initial data properly', () => {
    expect(users(undefined, { type: "LOAD_USER_REQUEST", payload: "" })).toEqual({
      data: [],
      isLoading: false,
      isFailed: false,
      nextLink: '/users?since=1'
    });
  });

  it('sets users data properly', () => {
    const action = getUsersSuccess(payload);
    stateAfterSuccessfullLoad = users(initialState, action)
    expect(stateAfterSuccessfullLoad).toEqual({
      data: payload.data,
      isLoading: false,
      isFailed: false,
      nextLink: "",
    });
  });

  it('sets loading data after request init', () => {
    const action = getUsersRequest();
    expect(users(initialState, action)).toMatchObject({
      isLoading:  true,
      isFailed: false,
    });
  });

  it('sets loading data after request failue', () => {
    const action = getUsersFailure();
    expect(users(initialState, action)).toMatchObject({
      isLoading:  false,
      isFailed: true,
    });
  });

  it('properly adds more users to list', () => {
    const action = getUsersSuccess(payload);
    const newState = users(stateAfterSuccessfullLoad, action);
    expect(newState.data).toHaveLength(4);
  });
});
