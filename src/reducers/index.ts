import { combineReducers } from 'redux';
import users from './users';
import user from './user';
import { IUser } from "../domain";

const rootReducer = combineReducers({
  users,
  user
});

export default rootReducer;

export interface IUsersListReducer {
  isLoading: boolean;
  isFailed: boolean;
  data: IUser[];
  nextLink: string;
}

export interface IUserReducer {
  isLoading: boolean;
  isFailed: boolean;
  data: IUser | null;
}

export interface IStore {
  users: IUsersListReducer;
  user: IUserReducer;
}
