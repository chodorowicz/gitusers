import update from 'immutability-helper';
import { ActionType } from 'typesafe-actions';

import * as constants from '../actions/constants';
import * as actions from '../actions/actionsCreators';
import { IUser } from "../domain";
import { IUserReducer } from ".";

export const initialState = {
  isLoading: false,
  isFailed: false,
  data: null,
}

function loadUserRequestReducer(state: IUserReducer) {
  return update(state, {
    isLoading: { $set: true },
    isFailed: { $set: false },
  });
}

function loadUserSuccessReducer(state: IUserReducer, { payload }: { payload: IUser }) {
  return update(state, {
    isLoading: { $set: false},
    data: { $set: payload },
  });

}
function loadUserFailureReducer(state: IUserReducer) {
  return update(state, {
    isLoading: { $set: false},
    isFailed: { $set: true },
  });
}

export default function(state: IUserReducer = initialState, action: ActionType<typeof actions>) {
  switch(action.type) {
    case constants.LOAD_USER_REQUEST:
      return loadUserRequestReducer(state);
    case constants.LOAD_USER_SUCCESS:
      return loadUserSuccessReducer(state, action);
    case constants.LOAD_USER_FAILURE:
      return loadUserFailureReducer(state);
    default:
      return state;
  }
}
