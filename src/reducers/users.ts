import { ActionType } from 'typesafe-actions';
import update from 'immutability-helper';

import { IStore, IUsersListReducer } from ".";
import * as actions from "../actions/actionsCreators";
import * as constants from "../actions/constants";
import { IUsersPayload } from "../domain";

export const initialState = {
  isLoading: false,
  isFailed: false,
  data: [],
  nextLink: '/users?since=1',
}

function getUsersRequestReducer(state: IUsersListReducer) {
  return update(state, {
    isLoading: { $set: true },
    isFailed: { $set: false },
  });
}

function getUsersSuccessReducer(state: IUsersListReducer, { payload } : { payload: IUsersPayload } ) {
  return update(state, {
    isLoading: { $set: false },
    data: { $push: payload.data },
    nextLink: { $set: payload.nextLink },
  });
}

function getUsersFailureReducer(state: IUsersListReducer) {
  return update(state, {
    isLoading: { $set: false },
    isFailed: { $set: true },
  });
}

export default function(state: IUsersListReducer = initialState, action: ActionType<typeof actions>) {
  switch(action.type) {
    case constants.GET_USERS_REQUEST:
      return getUsersRequestReducer(state);
    case constants.GET_USERS_SUCCESS:
      return getUsersSuccessReducer(state, action);
    case constants.GET_USERS_FAILURE:
      return getUsersFailureReducer(state);
    default:
      return state;
  }
}

export function nextLinkSelector(state: IStore) {
  return state.users.nextLink;
}

export function getUserSelector(state: IStore, login: string) {
  return state.users.data.find(user => user.login === login);
}
