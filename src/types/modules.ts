// import { IParseLinkHeaderObject } from "../domain";


declare module "parse-link-header" {
  interface IParsedLink {
    page: string;
    per_page: string;
    rel: "next";
    url: string
  }


  interface IParseLinkHeaderObject {
    next: IParsedLink;
    prev: IParsedLink;
    last: IParsedLink;
  }

  interface ParseLinkHeader {
    (x: string): IParseLinkHeaderObject;
  }
  const x: ParseLinkHeader;
  export = x;
}
