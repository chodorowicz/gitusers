import { IUser } from "./user";

export interface IUsersPayload {
  data: IUser[];
  nextLink: string;
}
