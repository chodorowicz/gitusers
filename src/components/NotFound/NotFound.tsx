import * as React from "react";
import { Row, Col, Alert } from "antd";

export const NotFound = () => {
  return (
    <Row>
      <Col span={24}>
        <Alert type="warning" message="This page doesn't exist." />
      </Col>
    </Row>
  );
};
