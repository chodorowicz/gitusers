import * as React from "react";
import { Avatar, Card } from "antd";

import { IUser } from "../../domain";

interface IProps {
  user: IUser;
}

export const DetailsCard: React.SFC<IProps> = ({ user }) => {
  return (
    <Card title={`User: ${user.login}`}>
      <Avatar src={user.avatar_url} size={150} />
      <ul style={{ marginTop: "20px" }}>
        <li>id: {user.id}</li>
        <li>
          GitHub page:{" "}
          <a href={user.html_url} target="_blank">
            {user.html_url}
          </a>
        </li>
      </ul>
    </Card>
  );
};
