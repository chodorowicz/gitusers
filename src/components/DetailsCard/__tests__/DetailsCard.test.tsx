import * as React from 'react';
import { mount } from 'enzyme';

import { DetailsCard } from '../';
import { user } from "../../../../tests/__mocks__/user";

describe('<DetailsCard />', () => {
  it('properly renders user data', () => {
    const wrapper = mount(<DetailsCard user={user} />);
    expect(wrapper.text()).toContain(user.id);
    expect(wrapper.text()).toContain(user.login);
    expect(wrapper.text()).toContain(user.html_url);
  });
});
