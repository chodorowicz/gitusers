import { Alert, Col, Row} from "antd";
import * as React from "react";
import { connect } from "react-redux";
import { IStore } from "../../reducers";
import { getUsersRequest } from "../../actions/actionsCreators";
import { IUser } from "../../domain";
import { LoadingSpinner } from "../LoadingSpinner/";
import { UsersList } from "../UsersList/";
import { LoadMoreUsersRow } from "./LoadMoreUsersRow";

interface IConnectedProps {
  users: IUser[];
  isLoading: boolean;
  isFailed: boolean;
}

interface IConnectedDispatch {
  getUsersRequest: typeof getUsersRequest;
}

class App extends React.Component<IConnectedProps & IConnectedDispatch> {
  public componentDidMount() {
    if (this.props.users.length === 0) {
      this.props.getUsersRequest();
    }
  }

  public render() {
    const { users, isLoading, isFailed } = this.props;
    return (
      <div>
        <Row>
          <Col span={24}>
            {users.length > 0 && !isFailed && <UsersList users={users} />}
            {isFailed && <Alert type="error" message="There was a problem loading GitHub users" />}
            {isLoading && <LoadingSpinner />}
          </Col>
        </Row>
        {!isLoading && users.length > 0 && <LoadMoreUsersRow getUsersRequest={this.props.getUsersRequest} />}
      </div>
    );
  }
}

export const StartConnected =  connect<IConnectedProps, IConnectedDispatch, {}, IStore> ((state) => ({
  isFailed: state.users.isFailed,
  isLoading: state.users.isLoading,
  users: state.users.data,
}), { getUsersRequest })(App);
