import { Button, Col, Row } from "antd";
import * as React from "react";

interface IProps {
  getUsersRequest: () => void;
}

export const LoadMoreUsersRow: React.SFC<IProps> = (props) => {
  return (
    <Row className="App-loadMoreRow">
      <Col sm={24}>
        <Button type="primary" onClick={props.getUsersRequest} htmlType="button">Load More Users</Button>
      </Col>
    </Row>
  );
};
