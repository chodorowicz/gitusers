import * as React from "react";
import { mount } from "enzyme";
import { LoadMoreUsersRow } from "../LoadMoreUsersRow";

describe("<LoadMoreUsersRow />", () => {
  it("render properly", () => {
    const onButtonClick = jest.fn();
    const wrapper = mount(<LoadMoreUsersRow getUsersRequest={onButtonClick} />);
    expect(wrapper.find("button")).toHaveLength(1);
    wrapper.find("button").simulate("click");
    expect(onButtonClick).toBeCalled();
  });
});
