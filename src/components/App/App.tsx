import { Col, Row} from "antd";
import * as React from "react";

import "./App.css";

export const App: React.SFC = ({ children }): JSX.Element => {
  return (
    <div className="App">
      <Row justify="center">
        <Col span={24}>
          <h1 className="App-title">GitHub Users Listing</h1>
        </Col>
      </Row>
      {children}
    </div>
  );
};
