import * as React from "react";

import "./LoadingSpinner.css";

export const LoadingSpinner: React.SFC = () => {
  return (
    <div className="LoadingSpinner">
      <div className="LoadingSpinner-inner">
        <i className="fa fa-spinner" aria-hidden="true" />
      </div>
    </div>
  );
};
