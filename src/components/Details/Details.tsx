import * as React from "react";
import { connect } from "react-redux";
import { Alert, Button, Col, Row } from "antd";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";

import { IStore } from "../../reducers";
import { loadUserRequest } from "../../actions/actionsCreators";
import { IUser } from "../../domain";
import { DetailsCard } from "../DetailsCard/";
import { LoadingSpinner } from "../LoadingSpinner/";
import "./Details.css";

interface IConnectedProps {
  user: IUser | null;
  isLoading: boolean;
  isFailed: boolean;
}

interface IConnectedDispatch {
  loadUserRequest: typeof loadUserRequest;
}

class Details extends React.Component<
  IConnectedProps & IConnectedDispatch & RouteComponentProps<{ login: string }>
> {
  public componentDidMount() {
    this.props.loadUserRequest(this.props.match.params.login);
  }
  public render() {
    const { isLoading, user, match, isFailed } = this.props;
    const { login } = match.params;
    return (
      <div className="Details">
        <Row>
          <Col span={24}>
            {isLoading && <LoadingSpinner />}
            {!isLoading &&
              user && (
                <Row>
                  <Col span={12} offset={6}>
                    <DetailsCard user={user} />
                  </Col>
                </Row>
              )}
            {isFailed && (
              <Alert
                type="error"
                message={`User with login ${login} was not found.`}
                style={{ marginBottom: "20px" }}
              />
            )}
            <div className="Details-center">
              <Button htmlType="button" type="primary">
                <Link className="btn btn-primary" to="/">
                  Go Back
                </Link>
              </Button>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export const DetailsConnected = connect<
  IConnectedProps,
  IConnectedDispatch,
  {},
  IStore
>(
  state => ({
    isFailed: state.user.isFailed,
    isLoading: state.user.isLoading,
    user: state.user.data,
  }),
  { loadUserRequest },
)(withRouter(Details));
