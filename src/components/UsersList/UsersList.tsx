import { Button, Table } from "antd";
import * as React from "react";
import { Link } from "react-router-dom";

import { IUser } from "../../domain";
import "./UsersList.css";

interface IOwnProps {
  users: IUser[];
  headings?: string[];
}

const columns = [
  {
    key: "avatar",
    render: (user: IUser) => (
      <img src={user.avatar_url} width="75" alt={user.login} />
    ),
    title: "Avatar",
  },
  {
    dataIndex: "login",
    key: "login",
    title: "Login",
  },
  {
    align: "right" as "right",
    key: "actions",
    render: (user: IUser) => (
      <Button type="primary" htmlType="button">
        <Link to={`/details/${user.login}`}>Details</Link>
      </Button>
    ),
    title: "Actions",
  },
];

export const UsersList: React.SFC<IOwnProps> = ({ users }) => {
  return (
    <div className="UsersList">
      <Table
        columns={columns}
        dataSource={users}
        pagination={false}
        size="middle"
      />
    </div>
  );
};
