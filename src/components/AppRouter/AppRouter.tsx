import * as React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import { App } from "..";
import { DetailsConnected } from "../Details";
import { NotFound } from "../NotFound";
import { StartConnected } from "../Start";

export const AppRouter: React.SFC = () => {
  return (
    <BrowserRouter>
      <App>
        <Switch>
          <Route exact={true} path="/" component={StartConnected} />
          <Route path="/details/:login" component={DetailsConnected} />
          <Route component={NotFound} />
        </Switch>
      </App>
    </BrowserRouter>
  );
};
