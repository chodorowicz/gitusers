import { takeLatest, call, put, select } from "redux-saga/effects";
import axios from "axios";
import * as parseLinkHeader from "parse-link-header";
import * as constants from "../actions/constants";
import {
  getUsersSuccess,
  getUsersFailure,
  loadUserSuccess,
  loadUserFailure,
} from "../actions/actionsCreators";
import { nextLinkSelector, getUserSelector } from "../reducers/users";
import * as actions from "../actions/actionsCreators";
import { IStore } from "../reducers";

const axiosInstance = axios.create({
  baseURL: "https://api.github.com/",
});

function* getUsers() {
  try {
    const nextLink = yield select(nextLinkSelector);
    const result = yield call(axiosInstance.get, nextLink);
    const linkHeader = parseLinkHeader(result.headers.link);
    yield put(
      getUsersSuccess({
        data: result.data,
        nextLink: linkHeader.next.url,
      }),
    );
  } catch (error) {
    yield put(getUsersFailure());
  }
}

function* loadUser({ payload: login }: ReturnType<typeof actions.loadUserRequest>) {
  let user = yield select((state: IStore) => getUserSelector(state, login));
  try {
    if (!user) {
      const response = yield call(axiosInstance.get, `/users/${login}`);
      user = response.data;
    }
    yield put(loadUserSuccess(user));
  } catch (error) {
    yield put(loadUserFailure());
  }
}

actions.loadUserRequest

export default function* rootSaga() {
  yield takeLatest(constants.GET_USERS_REQUEST, getUsers);
  yield takeLatest(constants.LOAD_USER_REQUEST, loadUser);
}
