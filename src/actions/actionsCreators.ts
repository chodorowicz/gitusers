
import { action } from "typesafe-actions";
import * as constants from './constants';
import { IUser, IUsersPayload } from "../domain";

export const getUsersRequest = () => action(constants.GET_USERS_REQUEST)
export const getUsersSuccess = (data: IUsersPayload) => action(constants.GET_USERS_SUCCESS, data);
export const getUsersFailure = () => action(constants.GET_USERS_FAILURE);

export const loadUserRequest = (login: string) => action(constants.LOAD_USER_REQUEST, login);
export const loadUserSuccess = (data: IUser) => action(constants.LOAD_USER_SUCCESS, data);
export const loadUserFailure = () => action(constants.LOAD_USER_FAILURE);
